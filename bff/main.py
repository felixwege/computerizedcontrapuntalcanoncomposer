from flask import Flask, request, send_file, make_response

from src.api.storage import BUCKET_NAME, list_blobs, download_blob

app = Flask(__name__)


@app.route("/getCanon")
def get_canon():
    previous_canon_name = request.args.get("previous", default="", type=str)

    blobs = list_blobs(BUCKET_NAME)
    sorted_blobs = sorted(blobs, key=lambda blob: blob.updated)

    i_previous_canon = None
    for i, blob in enumerate(sorted_blobs):
        if blob.name == previous_canon_name:
            i_previous_canon = i
            break

    # fallback to "old" canon to make sure there are enough canons available before the next batch
    # is generated
    canon_generator_job_interval = 60
    next_canon_name = sorted_blobs[-canon_generator_job_interval].name
    if i_previous_canon is not None:
        next_canon_name = sorted_blobs[i_previous_canon + 1].name

    path_to_file = "/tmp/canon.midi"
    download_blob(BUCKET_NAME, next_canon_name, path_to_file)

    response = make_response(send_file(path_to_file, mimetype="audio/midi"))
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Expose-Headers", "*")
    response.headers.add("x-canon-uuid", next_canon_name)
    return response


if __name__ == "__main__":
    app.run(host="127.0.0.1", port=8080, debug=True)
