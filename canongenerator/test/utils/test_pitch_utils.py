import pytest

from src.model.midi_pitches import MidiPitch
from src.model.note import Note
from src.model.note_values import NoteValue
from src.model.scale import Scale, ScaleEnum
from src.utils.pitch_utils import (
    extract_downbeat_pitches,
    find_legal_pitches,
    filter_by_distance_to_prev_pitch,
    filter_by_distance_to_second_voice,
    find_suitable_pitches,
    find_filler_pitches,
    find_nearby_pitches,
    filter_dissonant_pitches,
    find_transitional_pitches,
)


def test_extract_downbeat_pitches():
    tests = [
        {
            "name": "quarter notes",
            "notes": [
                Note(NoteValue.QUARTER, MidiPitch.C5),
                Note(NoteValue.QUARTER, MidiPitch.D5),
            ],
            "expected": [MidiPitch.C5, MidiPitch.D5],
        },
        {
            "name": "half note",
            "notes": [
                Note(NoteValue.HALF, MidiPitch.C5),
            ],
            "expected": [MidiPitch.C5, MidiPitch.C5],
        },
        {
            "name": "eight notes",
            "notes": [
                Note(NoteValue.EIGTH, MidiPitch.C5),
                Note(NoteValue.EIGTH, MidiPitch.D5),
                Note(NoteValue.EIGTH, MidiPitch.E5),
                Note(NoteValue.EIGTH, MidiPitch.F5),
            ],
            "expected": [MidiPitch.C5, MidiPitch.E5],
        },
        {
            "name": "dotted quarter note",
            "notes": [
                Note(NoteValue.DOTTED_QUARTER, MidiPitch.C5),
                Note(NoteValue.EIGTH, MidiPitch.D5),
            ],
            "expected": [MidiPitch.C5, MidiPitch.C5],
        },
    ]
    for test in tests:
        notes = test["notes"]
        pitches = extract_downbeat_pitches(notes)
        assert pitches == test["expected"], test["name"]


@pytest.fixture
def candidate_pitches():
    return [
        MidiPitch.C4,
        MidiPitch.D4,
        MidiPitch.E4,
        MidiPitch.F4,
        MidiPitch.G4,
        MidiPitch.A4,
        MidiPitch.B4,
        MidiPitch.C5,
        MidiPitch.D5,
        MidiPitch.E5,
        MidiPitch.F5,
        MidiPitch.G5,
        MidiPitch.A5,
        MidiPitch.B5,
        MidiPitch.C6,
    ]


def test_find_legal_pitches():
    tests = [
        {
            "pitch": MidiPitch.C5,
            "expected": [
                MidiPitch.C4,
                MidiPitch.E4,
                MidiPitch.F4,
                MidiPitch.A4,
                MidiPitch.C5,
                MidiPitch.E5,
                MidiPitch.G5,
                MidiPitch.A5,
                MidiPitch.C6,
            ],
        },
        {
            "pitch": MidiPitch.D5,
            "expected": [
                MidiPitch.D4,
                MidiPitch.F4,
                MidiPitch.G4,
                MidiPitch.B4,
                MidiPitch.D5,
                MidiPitch.F5,
                MidiPitch.A5,
                MidiPitch.B5,
            ],
        },
        {
            "pitch": MidiPitch.E5,
            "expected": [
                MidiPitch.C4,
                MidiPitch.E4,
                MidiPitch.G4,
                MidiPitch.A4,
                MidiPitch.C5,
                MidiPitch.E5,
                MidiPitch.G5,
                MidiPitch.B5,
                MidiPitch.C6,
            ],
        },
        {
            "pitch": MidiPitch.F5,
            "expected": [
                MidiPitch.D4,
                MidiPitch.F4,
                MidiPitch.A4,
                MidiPitch.D5,
                MidiPitch.F5,
                MidiPitch.A5,
                MidiPitch.C6,
            ],
        },
        {
            "pitch": MidiPitch.G5,
            "expected": [
                MidiPitch.C4,
                MidiPitch.E4,
                MidiPitch.G4,
                MidiPitch.B4,
                MidiPitch.C5,
                MidiPitch.E5,
                MidiPitch.G5,
                MidiPitch.B5,
            ],
        },
        {
            "pitch": MidiPitch.A4,
            "expected": [
                MidiPitch.C4,
                MidiPitch.D4,
                MidiPitch.F4,
                MidiPitch.A4,
                MidiPitch.C5,
                MidiPitch.E5,
                MidiPitch.F5,
                MidiPitch.A5,
                MidiPitch.C6,
            ],
        },
        {
            "pitch": MidiPitch.B4,
            "expected": [
                MidiPitch.D4,
                MidiPitch.E4,
                MidiPitch.G4,
                MidiPitch.B4,
                MidiPitch.D5,
                MidiPitch.G5,
                MidiPitch.B5,
            ],
        },
    ]
    scale = Scale(MidiPitch.C5, ScaleEnum.MAJOR)
    for test in tests:
        pitch = test["pitch"]
        pitches = find_legal_pitches(scale, pitch)
        assert pitches == test["expected"], f"find_legal_pitches, ${pitch}"


# pylint: disable=redefined-outer-name
def test_filter_by_distance_to_prev_pitch(candidate_pitches):
    tests = [
        {
            "name": "C5",
            "prev_pitch": MidiPitch.C5,
            "expected": [
                MidiPitch.F4,
                MidiPitch.G4,
                MidiPitch.A4,
                MidiPitch.B4,
                MidiPitch.D5,
                MidiPitch.E5,
                MidiPitch.F5,
                MidiPitch.G5,
            ],
        },
        {
            "name": "F5",
            "prev_pitch": MidiPitch.F5,
            "expected": [
                MidiPitch.B4,
                MidiPitch.C5,
                MidiPitch.D5,
                MidiPitch.E5,
                MidiPitch.G5,
                MidiPitch.A5,
                MidiPitch.B5,
                MidiPitch.C6,
            ],
        },
    ]
    for test in tests:
        prev_pitch = test["prev_pitch"]
        pitches = filter_by_distance_to_prev_pitch(
            candidate_pitches,
            prev_pitch,
        )
        assert pitches == test["expected"], test["name"]


# pylint: disable=redefined-outer-name
def test_filter_by_distance_to_second_voice(candidate_pitches):
    tests = [
        {
            "name": "C5",
            "second_voice_pitch": MidiPitch.C5,
            "expected": [
                MidiPitch.D5,
                MidiPitch.E5,
                MidiPitch.F5,
                MidiPitch.G5,
                MidiPitch.A5,
                MidiPitch.B5,
                MidiPitch.C6,
            ],
        },
        {
            "name": "D5",
            "second_voice_pitch": MidiPitch.D5,
            "expected": [
                MidiPitch.E5,
                MidiPitch.F5,
                MidiPitch.G5,
                MidiPitch.A5,
                MidiPitch.B5,
                MidiPitch.C6,
            ],
        },
        {
            "name": "E5",
            "second_voice_pitch": MidiPitch.E5,
            "expected": [
                MidiPitch.G5,
                MidiPitch.A5,
                MidiPitch.B5,
                MidiPitch.C6,
            ],
        },
        {
            "name": "F5",
            "second_voice_pitch": MidiPitch.F5,
            "expected": [
                MidiPitch.G5,
                MidiPitch.A5,
                MidiPitch.B5,
                MidiPitch.C6,
            ],
        },
        {
            "name": "G4",
            "second_voice_pitch": MidiPitch.G4,
            "expected": [
                MidiPitch.A4,
                MidiPitch.B4,
                MidiPitch.C5,
                MidiPitch.D5,
                MidiPitch.E5,
                MidiPitch.F5,
                MidiPitch.G5,
                MidiPitch.A5,
                MidiPitch.B5,
                MidiPitch.C6,
            ],
        },
        {
            "name": "BA",
            "second_voice_pitch": MidiPitch.A4,
            "expected": [
                MidiPitch.B4,
                MidiPitch.C5,
                MidiPitch.D5,
                MidiPitch.E5,
                MidiPitch.F5,
                MidiPitch.G5,
                MidiPitch.A5,
                MidiPitch.B5,
                MidiPitch.C6,
            ],
        },
        {
            "name": "B4",
            "second_voice_pitch": MidiPitch.B4,
            "expected": [
                MidiPitch.D5,
                MidiPitch.E5,
                MidiPitch.F5,
                MidiPitch.G5,
                MidiPitch.A5,
                MidiPitch.B5,
                MidiPitch.C6,
            ],
        },
    ]
    for test in tests:
        second_voice_pitch = test["second_voice_pitch"]
        pitches = filter_by_distance_to_second_voice(
            candidate_pitches,
            second_voice_pitch,
        )
        assert pitches == test["expected"], test["name"]


def test_find_suitable_pitches():
    tests = [
        {
            "name": "C4 E5",
            "second_voice_pitch": MidiPitch.C4,
            "prev_pitch": MidiPitch.E5,
            "expected": [
                MidiPitch.A4,
                MidiPitch.C5,
                MidiPitch.G5,
                MidiPitch.A5,
            ],
        },
        {
            "name": "C4 C5",
            "second_voice_pitch": MidiPitch.C4,
            "prev_pitch": MidiPitch.C5,
            "expected": [
                MidiPitch.G4,
                MidiPitch.A4,
                MidiPitch.E5,
                MidiPitch.G5,
            ],
        },
        {
            "name": "D4 B4",
            "second_voice_pitch": MidiPitch.D4,
            "prev_pitch": MidiPitch.B4,
            "expected": [
                MidiPitch.F4,
                MidiPitch.A4,
                MidiPitch.D5,
                MidiPitch.F5,
            ],
        },
    ]
    scale = Scale(MidiPitch.C5, ScaleEnum.MAJOR)
    for test in tests:
        second_voice_pitch = test["second_voice_pitch"]
        prev_pitch = test["prev_pitch"]
        pitches = find_suitable_pitches(scale, second_voice_pitch, prev_pitch)
        assert pitches == test["expected"], test["name"]


def test_find_filler_pitches():
    tests = [
        {
            "name": "fifth apart, ascending",
            "prev_pitch": MidiPitch.C5,
            "next_pitch": MidiPitch.G5,
            "expected": [MidiPitch.D5, MidiPitch.E5, MidiPitch.F5],
        },
        {
            "name": "fifth apart, descending",
            "prev_pitch": MidiPitch.G5,
            "next_pitch": MidiPitch.C5,
            "expected": [MidiPitch.D5, MidiPitch.E5, MidiPitch.F5],
        },
        {
            "name": "second apart",
            "prev_pitch": MidiPitch.G4,
            "next_pitch": MidiPitch.A4,
            "expected": [],
        },
    ]
    scale = Scale(MidiPitch.C5, ScaleEnum.MAJOR)
    for test in tests:
        prev_pitch = test["prev_pitch"]
        next_pitch = test["next_pitch"]
        pitches = find_filler_pitches(scale, prev_pitch, next_pitch)
        assert pitches == test["expected"], test["name"]


def test_find_nearby_pitches():
    tests = [
        {
            "name": "unison",
            "prev_pitch": MidiPitch.C5,
            "next_pitch": MidiPitch.C5,
            "expected": [
                MidiPitch.A4,
                MidiPitch.B4,
                MidiPitch.D5,
                MidiPitch.E5,
            ],
        },
        {
            "name": "second apart, ascending",
            "prev_pitch": MidiPitch.C5,
            "next_pitch": MidiPitch.D5,
            "expected": [MidiPitch.B4, MidiPitch.E5],
        },
        {
            "name": "second apart, descending",
            "prev_pitch": MidiPitch.D5,
            "next_pitch": MidiPitch.C5,
            "expected": [MidiPitch.B4, MidiPitch.E5],
        },
        {
            "name": "third apart, ascending",
            "prev_pitch": MidiPitch.C5,
            "next_pitch": MidiPitch.E5,
            "expected": [MidiPitch.D5],
        },
        {
            "name": "third apart, descending",
            "prev_pitch": MidiPitch.E5,
            "next_pitch": MidiPitch.C5,
            "expected": [MidiPitch.D5],
        },
    ]
    scale = Scale(MidiPitch.C5, ScaleEnum.MAJOR)
    for test in tests:
        prev_pitch = test["prev_pitch"]
        next_pitch = test["next_pitch"]
        pitches = find_nearby_pitches(scale, prev_pitch, next_pitch)
        assert pitches == test["expected"], test["name"]


# pylint: disable=redefined-outer-name
def test_filter_dissonant_pitches(candidate_pitches):
    tests = [
        {
            "name": "C5",
            "second_voice_pitch": MidiPitch.C5,
            "expected": [
                MidiPitch.C4,
                MidiPitch.D4,
                MidiPitch.E4,
                MidiPitch.F4,
                MidiPitch.G4,
                MidiPitch.A4,
                MidiPitch.C5,
                MidiPitch.D5,
                MidiPitch.E5,
                MidiPitch.F5,
                MidiPitch.G5,
                MidiPitch.A5,
                MidiPitch.C6,
            ],
        },
        {
            "name": "D5",
            "second_voice_pitch": MidiPitch.D5,
            "expected": [
                MidiPitch.C4,
                MidiPitch.D4,
                MidiPitch.E4,
                MidiPitch.F4,
                MidiPitch.G4,
                MidiPitch.A4,
                MidiPitch.B4,
                MidiPitch.C5,
                MidiPitch.D5,
                MidiPitch.E5,
                MidiPitch.F5,
                MidiPitch.G5,
                MidiPitch.A5,
                MidiPitch.B5,
                MidiPitch.C6,
            ],
        },
        {
            "name": "F5",
            "second_voice_pitch": MidiPitch.F5,
            "expected": [
                MidiPitch.C4,
                MidiPitch.D4,
                MidiPitch.F4,
                MidiPitch.G4,
                MidiPitch.A4,
                MidiPitch.C5,
                MidiPitch.D5,
                MidiPitch.F5,
                MidiPitch.G5,
                MidiPitch.A5,
                MidiPitch.C6,
            ],
        },
    ]
    for test in tests:
        second_voice_pitch = test["second_voice_pitch"]
        pitches = filter_dissonant_pitches(
            candidate_pitches,
            second_voice_pitch,
        )
        assert pitches == test["expected"], test["name"]


def test_find_transitional_pitches():
    tests = [
        {
            "name": "C major triad, ascending",
            "prev_pitch": MidiPitch.C5,
            "next_pitch": MidiPitch.G5,
            "second_voice_pitch": MidiPitch.C4,
            "expected": [MidiPitch.E5],
        },
        {
            "name": "C major triad, descending",
            "prev_pitch": MidiPitch.G5,
            "next_pitch": MidiPitch.C5,
            "second_voice_pitch": MidiPitch.C4,
            "expected": [MidiPitch.E5],
        },
        {
            "name": "D minor triad, ascending",
            "prev_pitch": MidiPitch.D5,
            "next_pitch": MidiPitch.A5,
            "second_voice_pitch": MidiPitch.C4,
            "expected": [MidiPitch.F5],
        },
        {
            "name": "D minor triad, descending",
            "prev_pitch": MidiPitch.A5,
            "next_pitch": MidiPitch.D5,
            "second_voice_pitch": MidiPitch.C4,
            "expected": [MidiPitch.F5],
        },
        {
            "name": "same note",
            "prev_pitch": MidiPitch.C5,
            "next_pitch": MidiPitch.C5,
            "second_voice_pitch": MidiPitch.C4,
            "expected": [MidiPitch.A4, MidiPitch.D5, MidiPitch.E5],
        },
        {
            "name": "second apart",
            "prev_pitch": MidiPitch.C5,
            "next_pitch": MidiPitch.D5,
            "second_voice_pitch": MidiPitch.C4,
            "expected": [MidiPitch.E5],
        },
        {
            "name": "fourth apart",
            "prev_pitch": MidiPitch.B4,
            "next_pitch": MidiPitch.E5,
            "second_voice_pitch": MidiPitch.A4,
            "expected": [MidiPitch.C5, MidiPitch.D5],
        },
        {
            "name": "fallback",
            "prev_pitch": MidiPitch.E5,
            "next_pitch": MidiPitch.G5,
            "second_voice_pitch": MidiPitch.B5,
            "expected": [MidiPitch.G5],
        },
    ]
    scale = Scale(MidiPitch.C5, ScaleEnum.MAJOR)
    for test in tests:
        prev_pitch = test["prev_pitch"]
        next_pitch = test["next_pitch"]
        second_voice_pitch = test["second_voice_pitch"]
        pitches = find_transitional_pitches(
            scale,
            prev_pitch,
            next_pitch,
            second_voice_pitch,
        )
        assert pitches == test["expected"], test["name"]
