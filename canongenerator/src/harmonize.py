import random
from typing import List

from src.model.bar import Bar
from src.model.intervals import Interval
from src.model.midi_pitches import MidiPitch
from src.model.scale import Scale
from src.utils.bar_utils import create_bar_from_rythm_and_downbeat_pitches
from src.utils.pitch_utils import extract_downbeat_pitches, find_suitable_pitches
from src.utils.rythm_utils import maybe_create_distinct_rythm
from src.utils.transpose import transpose_up


def harmonize(scale: Scale, second_voice_bar: Bar) -> Bar:
    return create_bar_from_rythm_and_downbeat_pitches(
        maybe_create_distinct_rythm(second_voice_bar.rythm),
        harmonize_downbeat(scale, second_voice_bar),
    )


def harmonize_downbeat(scale: Scale, second_voice_bar: Bar) -> List[MidiPitch]:
    downbeat_pitches = extract_downbeat_pitches(second_voice_bar.notes)
    prev_downbeat = transpose_up(
        downbeat_pitches[-1],
        Interval.OCTAVE,
    )
    harmonized_downbeat: List[MidiPitch] = []

    for second_voice_pitch in downbeat_pitches:
        harmony = random.choice(
            find_harmonies(
                scale,
                second_voice_pitch,
                prev_downbeat,
            )
        )
        harmonized_downbeat.append(harmony)
        prev_downbeat = harmonized_downbeat[-1]

    return harmonized_downbeat


def find_harmonies(
    scale: Scale,
    second_voice_pitch: MidiPitch,
    prev_downbeat: MidiPitch,
) -> List[MidiPitch]:
    suitable_pitches = find_suitable_pitches(
        scale,
        second_voice_pitch,
        prev_downbeat,
    )

    if len(suitable_pitches) != 0:
        return suitable_pitches

    return [transpose_up(second_voice_pitch, Interval.OCTAVE)]
