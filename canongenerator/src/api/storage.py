from typing import Optional

from google.cloud import storage  # type: ignore

from src.model.canon_metadata import CanonMetadata, metadata_from_dict


BUCKET_NAME = "cccc-dev.appspot.com"


def list_blobs(bucket_name):
    storage_client = storage.Client()
    blobs = storage_client.list_blobs(bucket_name)
    return blobs


def upload_blob(
    bucket_name,
    source_file_name,
    destination_blob_name,
    metadata: CanonMetadata,
):
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    blob.metadata = metadata.to_dict()
    blob.upload_from_filename(source_file_name)


def get_previous_canon_metadata(bucket_name) -> Optional[CanonMetadata]:
    blobs = list_blobs(bucket_name)
    sorted_blobs = sorted(blobs, key=lambda blob: blob.updated)
    previous_canon_name = sorted_blobs[-1].name
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.get_blob(previous_canon_name)
    metadata = metadata_from_dict(blob.metadata)
    return metadata
