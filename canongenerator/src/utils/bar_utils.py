from typing import List

from src.model.bar import Bar
from src.model.midi_pitches import MidiPitch
from src.model.note import Note
from src.model.note_values import NoteValue
from src.model.rythm import Rythm


def create_bar_from_rythm_and_downbeat_pitches(
    rythm: Rythm, pitches: List[MidiPitch]
) -> Bar:
    notes = []
    time, i = 0, 0
    for value in rythm.values:
        if time != 0 and time % NoteValue.QUARTER.value == 0:
            i += 1
        notes.append(Note(value, pitches[i]))
        time += value.value
    return Bar(notes)


def hold_for_duration(target_pitch: MidiPitch, value: NoteValue) -> Bar:
    return Bar([Note(value, target_pitch)])


def create_pause(value: NoteValue) -> Bar:
    return Bar([Note(value, None)])  # type: ignore
