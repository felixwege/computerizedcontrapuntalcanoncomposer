import random

from typing import List

from src.model.note_values import NoteValue
from src.model.rythm import Rythm


# create a rythm that contains anything at all
def create_rythm() -> Rythm:
    return Rythm(maybe_subdivide_half_note())


# create a rythm that contains something other than a half note
def create_exciting_rythm() -> Rythm:
    return Rythm(subdivide_half_note())


# create a rythm containing a half note
def create_boring_rythm() -> Rythm:
    return Rythm([NoteValue.HALF])


# create a rythm that has a different rythm from a given rythm
def maybe_create_distinct_rythm(prev_rythm: Rythm) -> Rythm:
    while True:
        new_rythm = create_rythm()
        if new_rythm != prev_rythm or random.random() > 0.8:
            return new_rythm


# create a rythm that has a different rythm from a given rythm and contains
# something other than a half note
def maybe_create_distinct_exciting_rythm(prev_rythm: Rythm) -> Rythm:
    while True:
        new_rythm = create_exciting_rythm()
        if new_rythm != prev_rythm or random.random() > 0.8:
            return new_rythm


def maybe_subdivide_half_note() -> List[NoteValue]:
    if random.random() < 0.2:
        # keep half note
        return [NoteValue.HALF]

    # subdivide half note
    return subdivide_half_note()


def subdivide_half_note() -> List[NoteValue]:
    rand = random.random()
    if rand < 0.4:
        # subdivide half note into two quarter notes, maybe subdivide the first one
        return maybe_subdivide_quarter_notes()

    if rand < 0.6:
        # subdivide half note into dotted quarter note and eigth note
        return [NoteValue.DOTTED_QUARTER, NoteValue.EIGTH]

    # subdivide half note into four eigth notes
    return subdivide_by(NoteValue.HALF, 4)


def maybe_subdivide_quarter_notes() -> List[NoteValue]:
    quarter_notes = subdivide_by(NoteValue.HALF, 2)
    if random.random() < 0.4:
        # subdivide second quarter note into two eigth notes
        return quarter_notes[:1] + subdivide_quarter_note()

    # keep both quarter notes
    return quarter_notes


def subdivide_quarter_note() -> List[NoteValue]:
    return subdivide_by(NoteValue.QUARTER, 2)


def subdivide_by(note_value, n_subdivisions) -> List[NoteValue]:
    new_value = NoteValue(note_value.value / n_subdivisions)
    return [new_value for _ in range(n_subdivisions)]
