from typing import List

from src.model.intervals import Interval, get_interval
from src.model.midi_pitches import MidiPitch
from src.model.note import Note
from src.model.note_values import NoteValue
from src.model.scale import Scale


def extract_downbeat_pitches(notes: List[Note]) -> List[MidiPitch]:
    downbeat_pitches: List[MidiPitch] = []
    time = 0
    for note in notes:
        if time % 1 == 0 and note.pitch is not None:
            downbeat_pitches.append(note.pitch)
        if note.value == NoteValue.HALF or note.value == NoteValue.DOTTED_QUARTER:
            downbeat_pitches.append(note.pitch)
        time += note.value.to_midi()
    return downbeat_pitches


def find_suitable_pitches(
    scale: Scale,
    second_voice_pitch: MidiPitch,
    prev_pitch: MidiPitch,
) -> List[MidiPitch]:
    return filter_by_distance_to_prev_pitch(
        filter_by_distance_to_second_voice(
            find_legal_pitches(scale, second_voice_pitch),
            second_voice_pitch,
        ),
        prev_pitch,
    )


def find_legal_pitches(scale: Scale, second_voice_pitch: MidiPitch) -> List[MidiPitch]:
    return [
        pitch
        for pitch in scale.upper_register
        if get_interval(second_voice_pitch, pitch).is_euphonious()
    ]


def filter_by_distance_to_prev_pitch(
    pitches: List[MidiPitch],
    prev_pitch: MidiPitch,
) -> List[MidiPitch]:
    return [
        pitch
        for pitch in pitches
        if pitch != prev_pitch
        and abs(pitch.value - prev_pitch.value) <= Interval.FIFTH.value
    ]


def filter_by_distance_to_second_voice(
    pitches: List[MidiPitch],
    second_voice_pitch: MidiPitch,
) -> List[MidiPitch]:
    return [
        pitch
        for pitch in pitches
        if pitch.value > second_voice_pitch.value
        and abs(pitch.value - second_voice_pitch.value) > Interval.MINOR_SECOND.value
    ]


def filter_dissonant_pitches(
    pitches: List[MidiPitch],
    reference: MidiPitch,
) -> List[MidiPitch]:
    if reference is None:
        return pitches

    filtered: List[MidiPitch] = []
    for pitch in pitches:
        if get_interval(reference, pitch).is_dissonant():
            continue
        filtered.append(pitch)
    return filtered


def find_filler_pitches(
    scale: Scale,
    prev_pitch: MidiPitch,
    next_pitch: MidiPitch,
) -> List[MidiPitch]:
    def sign(first: int, second: int, reference: int) -> float:
        return (second - reference) * (first - reference)

    return [
        pitch
        for pitch in scale.upper_register
        if sign(prev_pitch.value, next_pitch.value, pitch.value) < 0
    ]


def find_nearby_pitches(
    scale: Scale,
    prev_pitch: MidiPitch,
    next_pitch: MidiPitch,
) -> List[MidiPitch]:
    return [
        pitch
        for pitch in scale.upper_register
        if pitch != prev_pitch
        and pitch != next_pitch
        and abs(pitch.value - prev_pitch.value) <= Interval.MAJOR_THIRD.value
        and abs(pitch.value - next_pitch.value) <= Interval.MAJOR_THIRD.value
    ]


def find_transitional_pitches(
    scale: Scale,
    prev_pitch: MidiPitch,
    next_pitch: MidiPitch,
    second_voice_pitch: MidiPitch,
) -> List[MidiPitch]:
    filler_pitches = filter_dissonant_pitches(
        find_filler_pitches(scale, prev_pitch, next_pitch),
        second_voice_pitch,
    )

    if len(filler_pitches) == 3:
        # arpeggiate the triad
        return [filler_pitches[1]]

    if len(filler_pitches) != 0:
        # play something between prev and next pitch
        return filler_pitches

    nearby_pitches = filter_dissonant_pitches(
        find_nearby_pitches(scale, prev_pitch, next_pitch),
        second_voice_pitch,
    )

    if len(nearby_pitches) != 0:
        # play a nearby pitch
        return nearby_pitches

    # anticipate next_pitch
    return [next_pitch]
