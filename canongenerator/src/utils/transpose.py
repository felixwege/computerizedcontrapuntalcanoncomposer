from src.model.intervals import Interval
from src.model.midi_pitches import MidiPitch


def transpose_up(pitch: MidiPitch, interval: Interval) -> MidiPitch:
    return MidiPitch(pitch.value + interval.value)


def transpose_down(pitch: MidiPitch, interval: Interval) -> MidiPitch:
    return MidiPitch(pitch.value - interval.value)
