import random

from typing import List, Tuple

from src.model.canon import Canon
from src.model.intervals import Interval
from src.model.midi_pitches import MidiPitch
from src.model.note import Note
from src.model.note_values import NoteValue
from src.model.scale import Scale
from src.utils.transpose import transpose_down
from src.utils.pitch_utils import (
    find_transitional_pitches,
    find_legal_pitches,
    filter_by_distance_to_prev_pitch,
)


def polish_canon(canon: Canon, scale: Scale):
    first_voice_notes = canon.first_voice.flat()
    second_voice_notes = canon.second_voice.flat()
    i_1, t_1, i_2, t_2 = 0, 0.0, 0, 0.0
    duration = canon.n_bars() * NoteValue.HALF.value
    n_steps = int(duration / NoteValue.EIGTH.value)
    for step in range(n_steps):
        time = step * NoteValue.EIGTH.value
        n_1, i_1, t_1 = next_step(first_voice_notes, i_1, t_1, time)
        _, i_2, t_2 = next_step(second_voice_notes, i_2, t_2, time)

        if time % NoteValue.QUARTER.value == 0:
            # not on offbeat
            continue

        if n_1.value != NoteValue.EIGTH:
            continue

        if i_1 + 1 < len(first_voice_notes):
            new_pitch = find_transition(
                scale,
                first_voice_notes[i_1 - 1].pitch,
                first_voice_notes[i_1 + 1].pitch,
                second_voice_notes[i_2].pitch,
            )
            first_voice_notes[i_1].pitch = new_pitch
            if i_1 + 1 < len(second_voice_notes):
                second_voice_notes[i_1 + 1].pitch = transpose_down(
                    new_pitch,
                    Interval.OCTAVE,
                )
            continue

        # this is the last eigth note of the canon
        new_pitch = random.choice(
            find_final_pitch_candidates(
                scale,
                first_voice_notes[i_1 - 1].pitch,
                second_voice_notes[i_2].pitch,
            )
        )
        first_voice_notes[i_1].pitch = new_pitch


def next_step(
    notes: List[Note],
    i: int,
    prev_time: float,
    time: float,
) -> Tuple[Note, int, float]:
    note = notes[i]
    if time >= prev_time + note.value.value:
        return (notes[i + 1], i + 1, time)
    return (note, i, prev_time)


def find_transition(
    scale: Scale,
    prev_pitch: MidiPitch,
    next_pitch: MidiPitch,
    second_voice_pitch: MidiPitch,
) -> MidiPitch:
    return random.choice(
        find_transitional_pitches(
            scale,
            prev_pitch,
            next_pitch,
            second_voice_pitch,
        )
    )


def find_final_pitch_candidates(
    scale: Scale,
    prev_pitch: MidiPitch,
    second_voice_pitch: MidiPitch,
) -> List[MidiPitch]:
    candidates = filter_by_distance_to_prev_pitch(
        find_legal_pitches(scale, second_voice_pitch),
        prev_pitch,
    )

    if len(candidates) != 0:
        return candidates

    return [prev_pitch]
