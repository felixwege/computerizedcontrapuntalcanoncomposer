from enum import Enum
from typing import List

import random

from src.model.midi_pitches import MidiPitch
from src.model.intervals import Interval, get_interval


class ScaleEnum(Enum):
    MAJOR = [
        Interval.UNISON,
        Interval.MAJOR_SECOND,
        Interval.MAJOR_THIRD,
        Interval.FOURTH,
        Interval.FIFTH,
        Interval.MAJOR_SIXTH,
        Interval.MAJOR_SEVENTH,
    ]  # aka Ionian
    MINOR = [
        Interval.UNISON,
        Interval.MAJOR_SECOND,
        Interval.MINOR_THIRD,
        Interval.FOURTH,
        Interval.FIFTH,
        Interval.MINOR_SIXTH,
        Interval.MINOR_SEVENTH,
    ]  # aka Aeolian
    MINOR_PENTATONIC = [
        Interval.UNISON,
        Interval.MINOR_THIRD,
        Interval.FOURTH,
        Interval.FIFTH,
        Interval.MINOR_SEVENTH,
    ]
    PHRYGIAN = [
        Interval.UNISON,
        Interval.MINOR_SECOND,
        Interval.MINOR_THIRD,
        Interval.FOURTH,
        Interval.FIFTH,
        Interval.MINOR_SIXTH,
        Interval.MINOR_SEVENTH,
    ]  # major scale but starting at third
    BLUES = [
        Interval.UNISON,
        Interval.MINOR_THIRD,
        Interval.FOURTH,
        Interval.TRITONE,
        Interval.FIFTH,
        Interval.MINOR_SEVENTH,
    ]  # minor pentatonic plus tritone
    WHOLE_TONE = [
        Interval.UNISON,
        Interval.MAJOR_SECOND,
        Interval.MAJOR_THIRD,
        Interval.TRITONE,
        Interval.MINOR_SIXTH,
        Interval.MINOR_SEVENTH,
    ]  # stack whole steps
    OCTATONIC = [
        Interval.UNISON,
        Interval.MAJOR_SECOND,
        Interval.MINOR_THIRD,
        Interval.FOURTH,
        Interval.TRITONE,
        Interval.MINOR_SIXTH,
        Interval.MAJOR_SIXTH,
        Interval.MAJOR_SEVENTH,
    ]  # stack whole steps and half steps alternatingly
    AUGMENTED = [
        Interval.UNISON,
        Interval.MINOR_THIRD,
        Interval.MAJOR_THIRD,
        Interval.FIFTH,  # comment to fool pylint duplicate-code check
        Interval.MINOR_SIXTH,
        Interval.MAJOR_SIXTH,
    ]  # intersection of two augmented triads a minor third apart

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return self.__str__()


class Scale:
    def __init__(self, root: MidiPitch, scale_enum: ScaleEnum):
        self.root = root
        self.scale_enum = scale_enum
        self.pitches = create_scale_pitches(root, scale_enum)
        self.upper_register = self.__fill_register()

    def __src__(self):
        return f"root: ${self.root}, pitches: ${self.pitches}"

    def __repr__(self):
        return self.__str__()

    def __fill_register(self) -> List[MidiPitch]:
        upper_register: List[MidiPitch] = [
            pitch
            for pitch in self.pitches
            if pitch.value >= self.root.value - 1 * Interval.OCTAVE.value
            and pitch.value <= self.root.value + 1 * Interval.OCTAVE.value
        ]
        return upper_register


def create_scale_pitches(
    root: MidiPitch,
    scale_enum: ScaleEnum,
) -> List[MidiPitch]:
    lowest_root = MidiPitch(root.value % 12)
    pitches = [
        pitch
        for pitch in MidiPitch
        if get_interval(lowest_root, pitch) in scale_enum.value
    ]
    return pitches


def randomize_root() -> MidiPitch:
    return random.choice(
        [
            MidiPitch.C5,
            MidiPitch.C5_SHARP,
            MidiPitch.D5,
            MidiPitch.D5_SHARP,
            MidiPitch.E5,
            MidiPitch.F4,
            MidiPitch.F4_SHARP,
            MidiPitch.G4,
            MidiPitch.G4_SHARP,
            MidiPitch.A4,
            MidiPitch.A4_SHARP,
            MidiPitch.B4,
        ]
    )


def modulate(scale: Scale) -> Scale:
    circle_of_fifth = [
        MidiPitch.C5,
        MidiPitch.G4,
        MidiPitch.D5,
        MidiPitch.A4,
        MidiPitch.E5,
        MidiPitch.B4,
        MidiPitch.F4_SHARP,
        MidiPitch.C5_SHARP,
        MidiPitch.G4_SHARP,
        MidiPitch.D5_SHARP,
        MidiPitch.A4_SHARP,
        MidiPitch.F4,
    ]
    i = 0
    for i, pitch in enumerate(circle_of_fifth):
        if pitch.value % 12 == scale.root.value % 12:
            break
    candidates = [
        circle_of_fifth[(i + 12 - 1) % 12],
        circle_of_fifth[i],
        circle_of_fifth[(i + 1) % 12],
    ]
    root = random.choice(candidates)
    return Scale(root, scale.scale_enum)
