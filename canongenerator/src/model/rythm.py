from typing import List

from src.model.note_values import NoteValue


class Rythm:
    def __init__(self, values: List[NoteValue]):
        self.values = values

    def __str__(self):
        return "Rythm: [" + ", ".join([str(value) for value in self.values]) + "]"

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        if len(self.values) != len(other.values):
            return False

        for value_self, value_other in zip(self.values, other.values):
            if value_self != value_other:
                return False

        return True

    def check_length(self) -> bool:
        # time signature is 2/4
        return sum([note.value for note in self.values]) == 0.5
