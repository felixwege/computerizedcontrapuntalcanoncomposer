from enum import Enum
from src.model.midi_pitches import MidiPitch


class Interval(Enum):
    UNISON = 0
    MINOR_SECOND = 1
    MAJOR_SECOND = 2
    MINOR_THIRD = 3
    MAJOR_THIRD = 4
    FOURTH = 5
    TRITONE = 6
    FIFTH = 7
    MINOR_SIXTH = 8
    MAJOR_SIXTH = 9
    MINOR_SEVENTH = 10
    MAJOR_SEVENTH = 11
    OCTAVE = 12

    def is_euphonious(self):
        return self in EUPHONIOUS_INTERVALS

    def is_chord_tone(self):
        return self in CHORD_INTERVALS

    def is_dissonant(self):
        return self in DISSONANT_INTERVALS


EUPHONIOUS_INTERVALS = [
    Interval.UNISON,
    Interval.MINOR_THIRD,
    Interval.MAJOR_THIRD,
    Interval.FIFTH,
    Interval.MINOR_SIXTH,
    Interval.MAJOR_SIXTH,
    Interval.OCTAVE,
]

CHORD_INTERVALS = [
    Interval.UNISON,
    Interval.MINOR_THIRD,
    Interval.MAJOR_THIRD,
    Interval.FIFTH,
    Interval.OCTAVE,
]

DISSONANT_INTERVALS = [
    Interval.MINOR_SECOND,
    Interval.TRITONE,
    Interval.MAJOR_SEVENTH,
]


def get_interval(note1: MidiPitch, note2: MidiPitch) -> Interval:
    if note2.value > note1.value:
        return Interval((note2.value - note1.value) % 12)

    return Interval((note1.value - note2.value) % 12)
