from enum import Enum


class MidiPitch(Enum):
    C_SUB_0 = 0
    C_SUB_0_SHARP = 1
    D_SUB_0_FLAT = 1
    D_SUB_0 = 2
    D_SUB_0_SHARP = 3
    E_SUB_0_FLAT = 3
    E_SUB_0 = 4
    F_SUB_0 = 5
    F_SUB_0_SHARP = 6
    G_SUB_0_FLAT = 6
    G_SUB_0 = 7
    G_SUB_0_SHARP = 8
    A_SUB_0_FLAT = 8
    A_SUB_0 = 9
    A_SUB_0_SHARP = 10
    B_SUB_0_FLAT = 10
    B_SUB_0 = 11

    C0 = 12
    C0_SHARP = 13
    D0_FLAT = 13
    D0 = 14
    D0_SHARP = 15
    E0_FLAT = 15
    E0 = 16
    F0 = 17
    F0_SHARP = 18
    G0_FLAT = 18
    G0 = 19
    G0_SHARP = 20
    A0_FLAT = 20
    A0 = 21
    A0_SHARP = 22
    B0_FLAT = 22
    B0 = 23

    C1 = 24
    C1_SHARP = 25
    D1_FLAT = 25
    D1 = 26
    D1_SHARP = 27
    E1_FLAT = 27
    E1 = 28
    F1 = 29
    F1_SHARP = 30
    G1_FLAT = 30
    G1 = 31
    G1_SHARP = 32
    A1_FLAT = 32
    A1 = 33
    A1_SHARP = 34
    B1_FLAT = 34
    B1 = 35

    C2 = 36
    C2_SHARP = 37
    D2_FLAT = 37
    D2 = 38
    D2_SHARP = 39
    E2_FLAT = 39
    E2 = 40
    F2 = 41
    F2_SHARP = 42
    G2_FLAT = 42
    G2 = 43
    G2_SHARP = 44
    A2_FLAT = 44
    A2 = 45
    A2_SHARP = 46
    B2_FLAT = 46
    B2 = 47

    C3 = 48
    C3_SHARP = 49
    D3_FLAT = 49
    D3 = 50
    D3_SHARP = 51
    E3_FLAT = 51
    E3 = 52
    F3 = 53
    F3_SHARP = 54
    G3_FLAT = 54
    G3 = 55
    G3_SHARP = 56
    A3_FLAT = 56
    A3 = 57
    A3_SHARP = 58
    B3_FLAT = 58
    B3 = 59

    C4 = 60
    C4_SHARP = 61
    D4_FLAT = 61
    D4 = 62
    D4_SHARP = 63
    E4_FLAT = 63
    E4 = 64
    F4 = 65
    F4_SHARP = 66
    G4_FLAT = 66
    G4 = 67
    G4_SHARP = 68
    A4_FLAT = 68
    A4 = 69
    A4_SHARP = 70
    B4_FLAT = 70
    B4 = 71

    C5 = 72
    C5_SHARP = 73
    D5_FLAT = 73
    D5 = 74
    D5_SHARP = 75
    E5_FLAT = 75
    E5 = 76
    F5 = 77
    F5_SHARP = 78
    G5_FLAT = 78
    G5 = 79
    G5_SHARP = 80
    A5_FLAT = 80
    A5 = 81
    A5_SHARP = 82
    B5_FLAT = 82
    B5 = 83

    C6 = 84
    C6_SHARP = 85
    D6_FLAT = 85
    D6 = 86
    D6_SHARP = 87
    E6_FLAT = 87
    E6 = 88
    F6 = 89
    F6_SHARP = 90
    G6_FLAT = 90
    G6 = 91
    G6_SHARP = 92
    A6_FLAT = 92
    A6 = 93
    A6_SHARP = 94
    B6_FLAT = 94
    B6 = 95

    C7 = 96
    C7_SHARP = 97
    D7_FLAT = 97
    D7 = 98
    D7_SHARP = 99
    E7_FLAT = 99
    E7 = 100
    F7 = 101
    F7_SHARP = 102
    G7_FLAT = 102
    G7 = 103
    G7_SHARP = 104
    A7_FLAT = 104
    A7 = 105
    A7_SHARP = 106
    B7_FLAT = 106
    B7 = 107

    C8 = 108
    C8_SHARP = 109
    D8_FLAT = 109
    D8 = 110
    D8_SHARP = 111
    E8_FLAT = 111
    E8 = 112
    F8 = 113
    F8_SHARP = 114
    G8_FLAT = 114
    G8 = 115
    G8_SHARP = 116
    A8_FLAT = 116
    A8 = 117
    A8_SHARP = 118
    B8_FLAT = 118
    B8 = 119

    C9 = 120
    C9_SHARP = 121
    D9_FLAT = 121
    D9 = 122
    D9_SHARP = 123
    E9_FLAT = 123
    E9 = 124
    F9 = 125
    F9_SHARP = 126
    G9_FLAT = 126
    G9 = 127

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return self.__str__()
