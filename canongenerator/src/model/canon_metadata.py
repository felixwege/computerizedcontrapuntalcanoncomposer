from typing import Dict, Optional

from src.model.bar import Bar
from src.model.midi_pitches import MidiPitch
from src.model.note import Note
from src.model.note_values import NoteValue
from src.model.scale import Scale, ScaleEnum


class CanonMetadata:
    # pylint: disable=too-few-public-methods
    def __init__(self, scale: Scale, last_bar: Bar):
        self.scale = scale
        self.last_bar = last_bar

    def to_dict(self) -> Dict[str, str]:
        scale = f"{self.scale.root},{self.scale.scale_enum}"

        last_bar = ""
        for note in self.last_bar.notes:
            last_bar += f"{note.value},{note.pitch};"

        return {"scale": scale, "last_bar": last_bar}


def metadata_from_dict(metadata) -> Optional[CanonMetadata]:
    try:
        scale = metadata["scale"]
        root, scale_enum = scale.split(",")
        root = MidiPitch[root]
        scale_enum = ScaleEnum[scale_enum]

        notes = metadata["last_bar"].split(";")
        last_bar = []
        for note in notes:
            try:
                value, pitch = note.split(",")
                value = NoteValue[value]
                pitch = MidiPitch[pitch]
                last_bar.append(Note(value, pitch))
            except Exception:  # pylint: disable=broad-except
                pass

        return CanonMetadata(Scale(root, scale_enum), Bar(last_bar))
    except Exception:  # pylint: disable=broad-except
        return None
