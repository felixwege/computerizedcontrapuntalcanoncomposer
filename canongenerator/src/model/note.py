from src.model.midi_pitches import MidiPitch
from src.model.note_values import NoteValue


class Note:
    def __init__(self, value: NoteValue, pitch: MidiPitch):
        self.value = value
        self.pitch = pitch

    def __str__(self):
        return f"value: ${self.value}, pitch: ${self.pitch}"

    def __repr__(self):
        return self.__str__()
