from typing import List

from src.model.bar import Bar
from src.model.note import Note


class Voice:
    def __init__(self):
        self.bars: List[Bar] = []

    def __str__(self):
        return "\n".join([str(note) for note in self.bars])

    def __repr__(self):
        return self.__str__()

    def add_bar(self, bar: Bar):
        self.bars.append(bar)

    def add_empty_bar(self):
        self.bars.append(Bar([]))

    def flat(self) -> List[Note]:
        return [note for bar in self.bars for note in bar.notes]

    def is_empty(self) -> bool:
        return len(self.bars) == 0
