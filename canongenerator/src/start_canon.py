from src.model.bar import Bar
from src.model.canon import Canon
from src.model.intervals import Interval
from src.model.note import Note
from src.utils.transpose import transpose_down


def start_canon(last_bar: Bar) -> Canon:
    canon = Canon()
    canon.second_voice.add_bar(
        Bar(
            [
                Note(note.value, transpose_down(note.pitch, Interval.OCTAVE))
                for note in last_bar.notes
            ]
        )
    )
    return canon
