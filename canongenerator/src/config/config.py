from typing import Dict

import yaml


class Config:
    # pylint: disable=too-few-public-methods
    def __init__(self, config: Dict = None):
        if config is not None:
            self.n_bars = config["n_bars"]
            self.n_canons = config["n_canons"]
            self.polish_canon = config["polish_canon"]
            self.add_bass = config["add_bass"]
            self.path_to_file = config["path_to_file"]


def load_config(path_to_config="config.yaml", overwrites: Dict = None) -> Config:
    with open(path_to_config, "r") as file:
        config: Dict = yaml.safe_load(file)
        if overwrites is not None:
            config = config | overwrites
        return Config(config)
