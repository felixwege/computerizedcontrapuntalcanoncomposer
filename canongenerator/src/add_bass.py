import random

from typing import List

from src.model.canon import Canon
from src.model.intervals import Interval, get_interval
from src.model.midi_pitches import MidiPitch
from src.model.note_values import NoteValue
from src.model.scale import Scale
from src.model.voice import Voice
from src.utils.bar_utils import hold_for_duration
from src.utils.pitch_utils import extract_downbeat_pitches
from src.utils.transpose import transpose_up, transpose_down


def add_bass(canon: Canon, scale: Scale):
    third_voice = Voice()

    # pretend bass starts on first note of second voice transposed down an
    # octave
    bass = transpose_down(
        canon.second_voice.bars[0].notes[0].pitch,
        Interval.OCTAVE,
    )

    for i in range(canon.n_bars()):
        first_voice_downbeats = extract_downbeat_pitches(
            canon.first_voice.bars[i].notes
        )
        second_voice_downbeats = extract_downbeat_pitches(
            canon.second_voice.bars[i].notes
        )
        bass = random.choice(
            find_bass_pitches(
                scale,
                first_voice_downbeats,
                second_voice_downbeats,
                bass,
            )
        )
        third_voice.add_bar(hold_for_duration(bass, NoteValue.HALF))

    canon.third_voice = third_voice


def find_bass_pitches(
    scale: Scale,
    first_voice_downbeats: List[MidiPitch],
    second_voice_downbeats: List[MidiPitch],
    prev_pitch: MidiPitch,
) -> List[MidiPitch]:
    candidates = [
        pitch
        for pitch in scale.pitches
        if is_bass_candidate(
            scale,
            pitch,
            first_voice_downbeats[0],
            second_voice_downbeats[0],
            prev_pitch,
        )
    ]

    if len(candidates) != 0:
        return candidates

    return [transpose_down(second_voice_downbeats[0], Interval.OCTAVE)]


def is_bass_candidate(
    scale: Scale,
    pitch: MidiPitch,
    first_voice_pitch: MidiPitch,
    second_voice_pitch: MidiPitch,
    prev_pitch: MidiPitch,
) -> bool:
    # pylint: disable=too-many-return-statements
    if (
        pitch.value
        < second_voice_pitch.value - Interval.OCTAVE.value - Interval.FIFTH.value
    ):
        # too far below second voice
        return False

    if pitch.value > second_voice_pitch.value - Interval.FIFTH.value:
        # too close to second voice
        return False

    if abs(pitch.value - prev_pitch.value) > Interval.OCTAVE.value:
        # too far away from previous pitch
        return False

    if pitch.value < scale.root.value - 3 * Interval.OCTAVE.value:
        # too low
        return False

    if pitch.value > scale.root.value - 1 * Interval.OCTAVE.value:
        # too high
        return False

    if not get_interval(pitch, first_voice_pitch).is_chord_tone():
        # doesn't harmonize with first voice
        return False

    if not get_interval(pitch, second_voice_pitch).is_chord_tone():
        # doesn't harmonize with second voice
        return False

    return True


def find_closest_fifth(
    tonic: MidiPitch,
    prev_pitch: MidiPitch,
) -> MidiPitch:
    up_a_fifth = transpose_up(tonic, Interval.FIFTH)
    down_a_fourth = transpose_down(tonic, Interval.FOURTH)
    if abs(up_a_fifth.value - prev_pitch.value) < abs(
        down_a_fourth.value - prev_pitch.value
    ):
        return up_a_fifth

    return down_a_fourth
