import random

from typing import List

from src.model.bar import Bar
from src.model.canon import Canon
from src.model.intervals import Interval
from src.model.midi_pitches import MidiPitch
from src.model.note_values import NoteValue
from src.model.scale import Scale
from src.utils.bar_utils import (
    create_bar_from_rythm_and_downbeat_pitches,
    hold_for_duration,
)
from src.utils.pitch_utils import (
    extract_downbeat_pitches,
    find_legal_pitches,
    filter_by_distance_to_prev_pitch,
    filter_dissonant_pitches,
)
from src.utils.rythm_utils import maybe_create_distinct_exciting_rythm
from src.utils.transpose import transpose_up, transpose_down


def finish_canon(canon: Canon, scale: Scale):
    root = find_closest_root(scale, canon.first_voice.bars[-1].notes[-1].pitch)
    canon.add_bar(
        harmonize_final_bar(scale, root, canon.second_voice.bars[-1]), should_copy=True
    )
    canon.first_voice.add_bar(hold_for_duration(root, NoteValue.WHOLE))
    canon.second_voice.add_bar(
        hold_for_duration(
            transpose_down(root, Interval.OCTAVE),
            NoteValue.HALF,
        )
    )


def find_closest_root(scale: Scale, reference: MidiPitch) -> MidiPitch:
    root = scale.root
    for pitch in scale.upper_register:
        if pitch.value % Interval.OCTAVE.value == root.value % Interval.OCTAVE.value:
            if abs(pitch.value - reference.value) < abs(root.value - reference.value):
                root = pitch
    return root


def harmonize_final_bar(scale: Scale, root: MidiPitch, second_voice_bar: Bar) -> Bar:
    downbeat_pitches = extract_downbeat_pitches(second_voice_bar.notes)
    prev_downbeat = transpose_up(
        downbeat_pitches[-1],
        Interval.OCTAVE,
    )
    pre_penultimate_pitch = random.choice(
        find_nice_pitches(
            scale,
            downbeat_pitches[-2],
            prev_downbeat,
            root,
        )
    )
    penultimate_pitch = random.choice(
        find_nice_pitches(
            scale,
            downbeat_pitches[-1],
            pre_penultimate_pitch,
            root,
        )
    )
    return create_bar_from_rythm_and_downbeat_pitches(
        maybe_create_distinct_exciting_rythm(second_voice_bar.rythm),
        [
            pre_penultimate_pitch,
            penultimate_pitch,
        ],
    )


def find_nice_pitches(
    scale: Scale,
    second_voice_pitch: MidiPitch,
    prev_pitch: MidiPitch,
    root: MidiPitch,
) -> List[MidiPitch]:
    nice_pitches = filter_dissonant_pitches(
        filter_dissonant_pitches(
            filter_by_distance_to_prev_pitch(
                filter_by_distance_to_prev_pitch(
                    find_legal_pitches(scale, second_voice_pitch),
                    prev_pitch,
                ),
                root,
            ),
            second_voice_pitch,
        ),
        root,
    )

    if len(nice_pitches) == 0:
        return filter_by_distance_to_prev_pitch(
            find_legal_pitches(scale, second_voice_pitch),
            root,
        )

    return nice_pitches
