from midiutil import MIDIFile  # type: ignore

from src.model.canon import Canon
from src.model.voice import Voice


def export_midi(canon: Canon, path_to_file):
    n_tracks = canon.n_voices()
    channel = 0
    tempo = 120  # In BPM
    volume = 100  # 0-127, as per the MIDI standard
    midi_file = MIDIFile(n_tracks)

    def add_voice(voice: Voice, track):
        midi_file.addTempo(track, 0, tempo)
        for i, bar in enumerate(voice.bars):
            time = 2 * i  # one bar is two beats
            for note in bar.notes:
                if note.pitch is not None:
                    midi_file.addNote(
                        track,
                        channel,
                        note.pitch.value,
                        time,
                        note.value.to_midi(),
                        volume,
                    )
                time += note.value.to_midi()

    add_voice(canon.first_voice, 0)
    add_voice(canon.second_voice, 1)
    if not canon.third_voice.is_empty():
        add_voice(canon.third_voice, 2)

    with open(path_to_file, "wb") as output_file:
        midi_file.writeFile(output_file)
