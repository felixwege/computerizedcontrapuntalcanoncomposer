import uuid

from typing import Optional
from flask import Flask, request

from src.add_bass import add_bass
from src.config.config import Config, load_config
from src.export_midi import export_midi
from src.harmonize import harmonize
from src.model.bar import Bar
from src.model.note import Note
from src.model.note_values import NoteValue
from src.model.canon_metadata import CanonMetadata
from src.model.scale import Scale, ScaleEnum, randomize_root, modulate
from src.polish_canon import polish_canon
from src.start_canon import start_canon
from src.api.storage import BUCKET_NAME, upload_blob, get_previous_canon_metadata

app = Flask(__name__)


@app.route("/generateCanon")
def handle_generate_canon():
    n_canons = request.args.get("n", default=1, type=int)
    config = load_config(
        overwrites={"n_canons": n_canons, "path_to_file": "/tmp/canon.midi"}
    )

    success = True
    for _ in range(config.n_canons):
        try:
            previous_metadata = get_previous_canon_metadata(BUCKET_NAME)
            blob_name = uuid.uuid4().hex
            new_metadata = generate_canon(config, previous_metadata)
            upload_blob(BUCKET_NAME, config.path_to_file, blob_name, new_metadata)
        except Exception as err:  # pylint: disable=broad-except
            success = False
            print(err)

    if success:
        return ("Success", 200)

    return ("Error", 500)


def generate_canon(config: Config, metadata: Optional[CanonMetadata]) -> CanonMetadata:
    if metadata is None:
        scale = Scale(randomize_root(), ScaleEnum.MAJOR)
        last_bar = Bar([Note(NoteValue.HALF, scale.root)])
        metadata = CanonMetadata(scale, last_bar)
    else:
        metadata.scale = modulate(metadata.scale)

    canon = start_canon(metadata.last_bar)
    while canon.n_bars() < config.n_bars:
        canon.add_bar(
            harmonize(metadata.scale, canon.second_voice.bars[-1]), should_copy=True
        )
    canon.add_bar(
        harmonize(metadata.scale, canon.second_voice.bars[-1]), should_copy=False
    )

    if config.polish_canon:
        polish_canon(canon, metadata.scale)

    if config.add_bass:
        add_bass(canon, metadata.scale)

    export_midi(canon, config.path_to_file)

    last_bar = canon.first_voice.bars[-1]
    return CanonMetadata(metadata.scale, last_bar)


if __name__ == "__main__":
    generate_canon(load_config(), None)
    # uncomment to start server locally
    # app.run(host="127.0.0.1", port=8080, debug=True)
