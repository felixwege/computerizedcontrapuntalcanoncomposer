# Computerized Contrapuntal Canon Composer

https://frontend-dot-cccc-dev.ey.r.appspot.com


### Inspiration

- https://www.youtube.com/watch?v=OiG_5HcuJnc


### Rules for Composing Canons

- Notes of each half bar of the first voice are copied to the second bar but an octave lower
- Thirds, fifths, and sixths are considered euphonious
- Second, fourths, and sevenths are considered dissonant
- No parallel fifths/octaves
- Avoid repeating notes to make melodies more interesting
- Avoid large jumps for more natural melodies


### Usage

Clone

```
git clone git@gitlab.com:felixwege/computerizedcontrapuntalcanoncomposer.git && cd computerizedcontrapuntalcanoncomposer/canongenerator
```

Install dependencies

```
pip install --user -r pythonRequirements.txt
```

Run

```
python main.py
```

The output file is called `canon.midi`.
