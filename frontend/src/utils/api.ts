const bffUrl = "https://bff-dot-cccc-dev.ey.r.appspot.com";

export const getCanon = async (
  previousCanonUuid: string | undefined
): Promise<Response> => {
  try {
    const url = `${bffUrl}/getCanon`;
    if (previousCanonUuid !== undefined && previousCanonUuid.length !== 0) {
      const response = await fetch(
        `${url}?` + new URLSearchParams({ previous: previousCanonUuid })
      );
      return response;
    }

    const response = await fetch(url);
    return response;
  } catch (error) {
    return Promise.reject(error);
  }
};
