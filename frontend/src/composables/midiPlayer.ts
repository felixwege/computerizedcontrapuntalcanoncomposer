import MidiPlayer from "midi-player-js";
import { Player } from "soundfont-player";

interface MidiEvent {
  name: string;
  noteName: string;
  velocity: number;
}

export const useMidiPlayer = (
  audioContext: AudioContext,
  instrument: Player
) => {
  const midiPlayer = new MidiPlayer.Player((event: MidiEvent) => {
    if (event.name === "Note on") {
      instrument
        .play(event.noteName, audioContext.currentTime, {
          gain: event.velocity / 100,
        })
        .stop(audioContext.currentTime + 1);
    }
  });

  const play = (buffer: ArrayBuffer) => {
    stop();
    midiPlayer.loadArrayBuffer(buffer);
    midiPlayer.play();
  };

  const stop = () => {
    instrument.stop();
    midiPlayer.stop();
  };

  return { midiPlayer, play, stop };
};
